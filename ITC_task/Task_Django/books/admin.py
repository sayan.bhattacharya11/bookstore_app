from django.contrib import admin

# Register your models here.
from .models import Author,Book,Subscription,Subscriber


@admin.register(Author)
class AuthorAdmin(admin.ModelAdmin):
	pass


@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
	pass


@admin.register(Subscriber)
class SubscriberAdmin(admin.ModelAdmin):
	pass


@admin.register(Subscription)
class SubscriptionAdmin(admin.ModelAdmin):
	pass

