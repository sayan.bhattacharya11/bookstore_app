from rest_framework_json_api import  serializers
from django.contrib.auth.models import User
from rest_framework_json_api import filters
from rest_framework_json_api import django_filters
from rest_framework_json_api.relations import ResourceRelatedField,PrimaryKeyRelatedField
from ..models.subscriber import Subscriber


class SubscriberSerializer(serializers.ModelSerializer):
	user = ResourceRelatedField(queryset=User.objects)

	class Meta:
		model = Subscriber
		fields = ["address","phone_number","user"]

