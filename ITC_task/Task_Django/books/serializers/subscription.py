from rest_framework_json_api import  serializers
from django.contrib.auth.models import User
from rest_framework_json_api.relations import ResourceRelatedField
from ..models.subscription import Subscription
from ..models.subscriber import Subscriber
from ..models.book import Book


class SubscriptionSerializer(serializers.ModelSerializer):
	subscriber = ResourceRelatedField(queryset=Subscriber.objects,many=False,required=True)
	book = ResourceRelatedField(queryset=Book.objects,many=False,required=True)
	due_amount = serializers.SerializerMethodField()

	class Meta:
		model = Subscription
		fields = ["subscriber","book","borrowed_date","amount_paid","days","returned","due_amount"]

	def get_due_amount(self, obj):
		return (obj.book.subscription_cost * obj.days) - obj.amount_paid
