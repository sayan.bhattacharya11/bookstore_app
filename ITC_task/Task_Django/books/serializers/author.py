from rest_framework_json_api import  serializers
from rest_framework_json_api.relations import ResourceRelatedField
from ..models.author import Author


class AuthorSerializer(serializers.ModelSerializer):
	# books = ResourceRelatedField(many=True, read_only=True)

	class Meta:
		model = Author
		fields = ["name","address"]



