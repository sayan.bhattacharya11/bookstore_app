from rest_framework_json_api import  serializers
from rest_framework_json_api.relations import ResourceRelatedField,PrimaryKeyRelatedField
from ..models.book import Book
from django.contrib.auth.models import User
from ..serializers.author import AuthorSerializer


class BookSerializer(serializers.ModelSerializer):
	included_serializers = {
		'author': AuthorSerializer
	}
	author = ResourceRelatedField(queryset = User.objects ,many=True,required=True)
	# subscription = ResourceRelatedField()

	class Meta:
		model = Book
		fields = ["title","description","count","subscription_cost","topic","author"]
