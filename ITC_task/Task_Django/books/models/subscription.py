import  datetime
from django.db import  models
from ..models.book import Book
from  ..models.subscriber import Subscriber


class Subscription(models.Model):
	subscriber = models.ForeignKey(Subscriber,on_delete=models.CASCADE)
	book = models.ForeignKey(Book,on_delete=models.CASCADE)
	borrowed_date = models.DateField()
	amount_paid = models.PositiveIntegerField()
	days = models.PositiveIntegerField()
	returned = models.BooleanField(default=False)


