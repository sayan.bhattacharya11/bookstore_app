from django.db import  models
from django.contrib.auth.models import User
from django.core.validators import RegexValidator


class Subscriber(models.Model):
	user = models.ForeignKey(User,on_delete=models.CASCADE)
	address = models.TextField()
	phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$')
	phone_number = models.CharField(validators=[phone_regex], max_length=17, blank=True)