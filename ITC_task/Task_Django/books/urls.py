from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from books import  views

router = DefaultRouter()
router.register('authors', views.AuthorViewset, 'Crud')
router.register('books', views.BookViewset, 'Crud_Book')
router.register('subscriber', views.SubscriberViewset, 'Crud_Subscriber')
router.register('subscription', views.SubscriptionViewset, 'Crud_Subscription')
router.register('expiry', views.SubscriptionExpiredViewset, 'expiry')



urlpatterns = [
    path('api/', include(router.urls))
]