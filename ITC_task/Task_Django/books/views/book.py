from rest_framework import viewsets
from rest_framework_json_api import filters
from rest_framework_json_api import django_filters
from ..models.book import Book
from ..serializers.book import BookSerializer


class BookViewset(viewsets.ModelViewSet):
	queryset = Book.objects.all()
	serializer_class = BookSerializer
	ordering = ["id"]
	filter_backends = (filters.QueryParameterValidationFilter, filters.OrderingFilter,
	                   django_filters.DjangoFilterBackend)
	filterset_fields = {
		'id': ('exact', 'lt', 'gt', 'gte', 'lte', 'in'),
		'title': ('icontains', 'iexact', 'contains'),
		'description': ('icontains', 'iexact', 'contains'),
		'author': ('icontains', 'iexact', 'contains'),

	}
	search_fields = ('id', 'title','description','author')