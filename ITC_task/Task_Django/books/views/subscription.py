import datetime
from rest_framework_json_api import filters
from rest_framework_json_api import django_filters
from rest_framework import viewsets
from rest_framework.response import Response


from ..models.subscription import Subscription
from ..serializers.subscription import SubscriptionSerializer


class SubscriptionViewset(viewsets.ModelViewSet):
	queryset = Subscription.objects.all()
	serializer_class = SubscriptionSerializer
	ordering = ["borrowed_date"]
	filter_backends = (filters.QueryParameterValidationFilter, filters.OrderingFilter,
	                   django_filters.DjangoFilterBackend)
	filterset_fields = {
		'id': ('exact', 'lt', 'gt', 'gte', 'lte', 'in'),
	}
	search_fields = ('id',)


class SubscriptionExpiredViewset(viewsets.ViewSet):

	def list(self, request):
		queryset = Subscription.objects.filter(returned=False)
		format_str = '%Y%m%d'
		qs = ""
		for element in queryset:
			date = str(element.borrowed_date).split("-")
			days_elapsed = int(date[2]) + element.days
			date.pop()
			date.append(str(days_elapsed))
			date_str = "".join(date)
			datetime_obj = datetime.datetime.strptime(date_str, format_str)
			difference = datetime.date.today() - datetime_obj.date()
			days = difference.days
			if days < 0:
				qs = queryset.exclude(id=element.id)
		serializer = SubscriptionSerializer(qs, many=True)
		return Response(serializer.data)



