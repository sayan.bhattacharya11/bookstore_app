from rest_framework import viewsets
from rest_framework_json_api import filters
from rest_framework_json_api import django_filters
from ..models.subscriber import Subscriber
from ..serializers.subscriber import SubscriberSerializer


class SubscriberViewset(viewsets.ModelViewSet):
	queryset = Subscriber.objects.all()
	serializer_class = SubscriberSerializer
	ordering = ["id"]
	filter_backends = (filters.QueryParameterValidationFilter, filters.OrderingFilter,
	                   django_filters.DjangoFilterBackend)
	filterset_fields = {
		'id': ('exact', 'lt', 'gt', 'gte', 'lte', 'in'),
		'user': ('icontains', 'iexact', 'contains'),
	}
	search_fields = ('id', 'user')


