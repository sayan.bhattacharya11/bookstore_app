from rest_framework import viewsets
from rest_framework_json_api import filters
from rest_framework_json_api import django_filters
from ..models.author import Author
from ..serializers.author import AuthorSerializer


class AuthorViewset(viewsets.ModelViewSet):
	queryset = Author.objects.all()
	serializer_class = AuthorSerializer
	ordering = ["id"]
	filter_backends = (filters.QueryParameterValidationFilter, filters.OrderingFilter,
	                   django_filters.DjangoFilterBackend)
	filterset_fields = {
		'id': ('exact', 'lt', 'gt', 'gte', 'lte', 'in'),
		'name': ('icontains', 'iexact', 'contains'),
	}
	search_fields = ('id', 'name')
